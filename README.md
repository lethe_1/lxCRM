#lxCRM
=====

我公司自己使用的CRM，基于C3CRM. https://github.com/dfar2008/c3crm.git 

向易客CRM致敬

##lxCRM有哪些功能？

* 客户管理
* 
* 
* 
* 
* 
* 
* 

##有问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* 邮件(liuxin#sceletech.com, 把#换成@)
* QQ: 395112951

##捐助开发者
在兴趣的驱动下,写一个`免费`的东西，有欣喜，也还有汗水，希望你喜欢我的作品，同时也能支持一下。
当然，有钱捧个钱场（右上角的爱心标志，支持支付宝和PayPal捐助），没钱捧个人场，谢谢各位。

##感激
感谢以下的项目,排名不分先后

* [易客CRM](https://github.com/dfar2008/c3crm) 
* [SuiteCRM](https://github.com/salesagility/SuiteCRM.git)
* [jquery](http://jquery.com)

##关于作者

```javascript
  var lethe = {
    nickName  : "lethe",
    site : "http://www.liuxin.site"
  }
```