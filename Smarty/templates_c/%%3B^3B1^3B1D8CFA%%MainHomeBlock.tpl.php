<?php /* Smarty version 2.6.18, created on 2015-12-30 06:13:12
         compiled from Home/MainHomeBlock.tpl */ ?>
	<!--<div id="stuff_<?php echo $this->_tpl_vars['tablestuff']['Stuffid']; ?>
" class="portlet" style="overflow-y:auto;overflow-x:hidden;height=280;width:<?php echo $this->_tpl_vars['tablestuff']['Width']; ?>
;float:left;position:relative">
	<table width="100%" cellpadding="0" cellspacing="0" class="small portlet_topper" style="padding-right:0px;padding-left:0px;padding-top:0px;">
		<tr id="headerrow_<?php echo $this->_tpl_vars['tablestuff']['Stuffid']; ?>
">			
		<td align="left" style="height:20px;" nowrap width=60%><b>&nbsp;<?php echo $this->_tpl_vars['tablestuff']['Stufftitle']; ?>
</b></td>
		<td align="right" style="height:20px;" width=5%>
		<span id="refresh_<?php echo $this->_tpl_vars['tablestuff']['Stuffid']; ?>
" style="position:relative;">&nbsp;&nbsp;</span>
		</td>
		<td align="right" style="height:20px;" width=35% nowrap>
			<a style='cursor:pointer;' onclick="loadStuff(<?php echo $this->_tpl_vars['tablestuff']['Stuffid']; ?>
,'<?php echo $this->_tpl_vars['tablestuff']['Stufftype']; ?>
');"><img src="<?php echo $this->_tpl_vars['IMAGE_PATH']; ?>
windowRefresh.gif" border="0" alt="<?php echo $this->_tpl_vars['APP']['LBL_REFRESH']; ?>
" title="<?php echo $this->_tpl_vars['APP']['LBL_REFRESH']; ?>
" hspace="2" align="absmiddle"/></a>
			
			<a id="deletelink" style='cursor:pointer;' onclick="DelStuff(<?php echo $this->_tpl_vars['tablestuff']['Stuffid']; ?>
)"><img src="<?php echo $this->_tpl_vars['IMAGE_PATH']; ?>
windowClose.gif" border="0" alt="<?php echo $this->_tpl_vars['APP']['LBL_CLOSE']; ?>
" title="<?php echo $this->_tpl_vars['APP']['LBL_CLOSE']; ?>
" hspace="5" align="absmiddle"/></a>			
		</td>
		</tr>
	</table>
		
	<table width="100%" cellpadding="0" cellspacing="0" class="small portlet_content" style="padding-right:0px;padding-left:0px;padding-top:0px;">
		<tr id="maincont_row_<?php echo $this->_tpl_vars['tablestuff']['Stuffid']; ?>
">	
			<td>
				<div id="stuffcont_<?php echo $this->_tpl_vars['tablestuff']['Stuffid']; ?>
" style="overflow-y: auto; overflow-x:hidden;width:100%;height:100%;cursor:auto;">
				</div>
			</td>
		</tr>
	</table>
</div>-->
<script language="javascript">
	//window.onresize = function(){positionDivInAccord('stuff_<?php echo $this->_tpl_vars['tablestuff']['Stuffid']; ?>
','<?php echo $this->_tpl_vars['tablestuff']['Width']; ?>
');};
	//positionDivInAccord('stuff_<?php echo $this->_tpl_vars['tablestuff']['Stuffid']; ?>
','<?php echo $this->_tpl_vars['tablestuff']['Width']; ?>
');
	function hiddenwin(divid){
		$("#"+divid).css("display","none");
	}
</script>	

<style>
<?php echo '
.mydash{
	background:none repeat scroll 0 0 #fff;
	cursor:pointer;
	float:left;
	height:280px;
	margin:10px 0 0 10px;
	margin-top:10px;
	-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.2);
	-moz-box-shadow:0 1px 3px rgba(0,0,0,0.2);
	box-shadow:0 1px 3px rgba(0,0,0,0.2);
}
.mydash h4{
	background: -moz-linear-gradient(center top , #FEFEFE, #F5F5F5) repeat-x scroll 0 0 #FAFAFA;
	border-bottom:1px solid #d4d4d4;
	color:#666;
	cursor:pointer;
	font-size:14px;
	font-weight:400;
	padding:0px 0 6px 10px;
	position:relative;
	background:#fafafa repeat-x;
	background-image:-webkit-gradient(linear,0,0,0 100%,from(#fefefe),to(whiteSmoke));
}
.mydash h4 span{
		display:none;
}
.mydash h4 span{
	position:absolute;
	right:10px;
	top:2px;
}

.mydash h4:hover span{
	display:inline;
}
'; ?>

</style>
<div style="width:33.3%;float:left;">
	<div style="width:97%;float:left;position:relative;display:block;overflow-y:auto;overflow-x:hidden;" class="mydash" id="<?php echo $this->_tpl_vars['val']['divid']; ?>
_win">
		<div style="background-color:#fafafa;padding-top:1px">
			<h4>
				<?php echo $this->_tpl_vars['val']['title']; ?>

				<span>
					<a href="#" hidefocus="true" onclick=""><i class="icon-repeat"></i></a>
					<a href="#" hidefocus="true" onclick="hiddenwin('<?php echo $this->_tpl_vars['val']['divid']; ?>
_win')"><i class="icon-remove"></i></a>
				</span>
			</h4>
		</div>
		<div style="margin-left:5px;margin-right:5px" >
			<?php if ($this->_tpl_vars['val']['type'] == 'text'): ?>
				<?php $_from = $this->_tpl_vars['val']['content']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['cont']):
?>
					<p><?php echo $this->_tpl_vars['cont']; ?>
</p>
				<?php endforeach; endif; unset($_from); ?>
			<?php elseif ($this->_tpl_vars['val']['type'] == 'table'): ?>
				<table class="table table-bordered table-condensed table-striped table-hover">
					<thead>
						<?php $_from = $this->_tpl_vars['val']['thead']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['heads']):
?>
						<td><?php echo $this->_tpl_vars['heads']; ?>
</td>
						<?php endforeach; endif; unset($_from); ?>
					</thead>
					<tbody>
						<?php echo $this->_tpl_vars['val']['tbody']; ?>

					</tbody>
				</table>
			<?php else: ?>
				<div id="<?php echo $this->_tpl_vars['val']['divid']; ?>
">
				<script>highchartss("<?php echo $this->_tpl_vars['val']['categorys']; ?>
",'<?php echo $this->_tpl_vars['val']['divid']; ?>
',"<?php echo $this->_tpl_vars['val']['series']; ?>
","<?php echo $this->_tpl_vars['val']['title']; ?>
","<?php echo $this->_tpl_vars['val']['type']; ?>
","<?php echo $this->_tpl_vars['val']['name']; ?>
");</script>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>