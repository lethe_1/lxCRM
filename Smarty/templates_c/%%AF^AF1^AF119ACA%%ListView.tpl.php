<?php /* Smarty version 2.6.18, created on 2015-12-29 20:06:03
         compiled from Home/ListView.tpl */ ?>

<!-- highcharts plugin-->
<script src="include/js/highcharts.js" type="text/javascript"></script>
<script type="text/javascript">

	function highchartss(category,divid,series,title,type,name){
		var chart;
		var cat_arr = new Array();
		cat_arr = category.split(",");
		if(type.split(",").length>1){
			type = type.split(",");
			series = series.split("_");
			name = name.split(",");
			$(document).ready(function() {
			var colors = Highcharts.getOptions().colors,
					categories = cat_arr,
				chart = new Highcharts.Chart({
					chart: {
						//renderTo: 'container',
						renderTo:divid,
						height:210,
						inverted: false  //左右显示，默认上下正向。假如设置为true，则横纵坐标调换位置
					},
					title: {
						text: title
						},
					xAxis: {
						categories: categories,
						labels: {
							rotation: 0   //坐标值显示的倾斜度  
						}     
					},
					yAxis: [{
						min: 0,
						title: {
							text: '数值(元)'
						}
					},{
						min:0,
						title:{
							text:"数值(个)"
						},
						opposite:true
					}
					],
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y ;
								}
							}
						}
					},
					tooltip: {
						crosshairs: true,
						formatter: function() {
							var s;
							if (type == 'pie') {// the pie chart
								s = ''+
									this.point.name + ': '+ this.percentage +' %';
							} else {
								s = ''+
									this.x  +': '+ this.y;
							}
							return s;
						}
					},
					series:[{
						name:name[0],
						type:type[0],
						data:eval("["+series[0]+"]")
					},
					{
						name:name[1],
						type:type[1],
						data:eval("["+series[1]+"]"),
						yAxis: 1
					}]
			
				});
		  });
	  }
	  else{
			$(document).ready(function() {
			var colors = Highcharts.getOptions().colors,
					categories = cat_arr,
				chart = new Highcharts.Chart({
					chart: {
						renderTo:divid,
						height:210,
						inverted: false  //左右显示，默认上下正向。假如设置为true，则横纵坐标调换位置
					},
					title: {
						text: title
						},
					xAxis: {
						categories: categories,
						labels: {
							rotation: 0   //坐标值显示的倾斜度  
						}     
					},
					yAxis: {
						min: 0,
						title: {
							text: '数值(元)'
						}
					}
					,
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.y ;
								}
							}
						}
					},
					tooltip: {
						crosshairs: true,
						formatter: function() {
							var s;
							if (type == 'pie') {// the pie chart
								s = ''+
									this.point.name + ': '+ this.percentage +' %';
							} else {
								s = ''+
									this.x  +': '+ this.y;
							}
							return s;
						}
					},
					series:[{
						name:name,
						type:type,
						data:eval("["+series+"]")
					}
					]
			
				});
		  });
		
	  }
	 }

</script>

<!-- Contents Start-->
<div style="height:820px;background-color:#F6F7F7;margin:0 auto;margin-top:-8px;margin-bottom:-30px;width:100%;position:relative"  >
	<br>
	<div style="width:90%;margin:0 auto;">
		<div class="summary"style="margin-left:10px">
			<div class="datalist">
				<ul>
					<li>
						<h3>本周新增客户数</h3>
						<h4 class="up"><?php echo $this->_tpl_vars['DAYTOTAL']; ?>
</h4>
						<div class="data">
							<p>
								较上周：
								<span>
								<?php if ($this->_tpl_vars['ACCOUNTUPDOWN'] == 'U'): ?>
								<i class="icon-arrow-up"></i>
								<?php else: ?>
								<i class="icon-arrow-down"></i>
								<?php endif; ?>
								<?php echo $this->_tpl_vars['ACCOUNTPERCENT']; ?>

								</span>
							</p>
						</div>
					</li>
					
					<li >
						<h3>本周新增联系记录数</h3>
						<h4><?php echo $this->_tpl_vars['WEEKNEWNOTES']; ?>
</h4>
						<div class="data">
							<p>
								较上周：<span>
								<?php if ($this->_tpl_vars['NOTESUPDOWN'] == 'U'): ?>
								<i class="icon-arrow-up"></i> 
								<?php else: ?>
								<i class="icon-arrow-down"></i> 
								<?php endif; ?>
								<?php echo $this->_tpl_vars['NOTESPERCENT']; ?>
</span>
							</p>
						</div>
					</li>
					<li style="border-right:1px solid #ededed;margin-left:-12px">
						<h3>本周订单成交量</h3>
						<h4 class="up"><?php echo $this->_tpl_vars['ORDERCOUNT']; ?>
</h4>
						<div class="data">
							<p>
								较上周：
								<span>
								<?php if ($this->_tpl_vars['ORDERCOUNTUPDOWN'] == 'D'): ?>
								<i class="icon-arrow-down"></i>
								<?php else: ?>
								<i class="icon-arrow-up"></i>
								<?php endif; ?>
								<?php echo $this->_tpl_vars['ORDERCOUNTPERCENT']; ?>

								</span>
							</p>
						</div>
					</li>
					<li>
						<h3>本周订单成交金额</h3>
						<h4 class="up"><?php echo $this->_tpl_vars['MONTHDEALORDER']; ?>
</h4>
						<div class="data">
							<p>
								较上周：
								<span style="display:inline">
								<?php if ($this->_tpl_vars['ORDERUPDOWN'] == 'D'): ?>
								<i class="icon-arrow-down"></i>
								<?php else: ?>
								<i class="icon-arrow-up"></i>
								<?php endif; ?>
								<?php echo $this->_tpl_vars['ORDERPERCENT']; ?>

								</span>
							</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="dash-container"style="width:90%;margin:0 auto">
	<?php $_from = $this->_tpl_vars['MYDASHBOARD']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['val']):
?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "Home/MainHomeBlock.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endforeach; endif; unset($_from); ?>
	</div>
</div>
<?php echo '
<style>
.dash-container{
	/*min-width:950px;
	max-width:1326px;*/
	
	height:400px;
	
}

.summary{
	background:#fff;
	padding:10px 0px;
    
	position:relative;
	-webkit-box-shadow:0 1px 2px rgba(0,0,0,0.2);
	-moz-box-shadow:0 1px 2px rgba(0,0,0,0.2);
	box-shadow:0 1px 2px rgba(0,0,0,0.2);
	border:1px solid #EDEDED\\9;
	/*min-width:950px;
	max-width:1274px;
	max-width:1272px\\9;
	_width:950px;*/
}
.summary h3{
	height: 34px;
	font-size: 16px;
	font-weight:400;
}

.datalist{
	/*width:530px;
	right:410px; */
	padding: 20px 0px 10px;
	height:148px;
	z-index:0;
	/*border-right:1px solid #ededed;
	margin-right:420px;*/
	
}
.datalist li{
	width:25%; 
	float:left;
	text-align:center;
}
.datalist h3{
	font-size:16px;
	font-weight:400;
	color:#666666;
}
.datalist h4{
	font-size:38px;
	font-weight:400;
	font-family:Arial;
	padding:8px 0;
	text-shadow:#fff 0 0 50px;
	color:#333333;
}
ul{
	list-style:none;
}

.datalist .up,.datalist .down{
	-webkit-animation-timing-function: ease-in-out;
	-webkit-animation-iteration-count: infinite;
	-webkit-animation-duration: 4s;
	-webkit-animation-direction: normal;
	-moz-animation-timing-function: ease-in-out;
	-moz-animation-iteration-count: infinite;
	-moz-animation-duration: 4s;
	-moz-animation-direction: normal;
}
.datalist .up{
	 -webkit-animation-name: fadeup;
	  -moz-animation-name: fadeup;
}
.datalist .down{
	-webkit-animation-name: fadedown;
	 -moz-animation-name: fadedown;
}

.datalist .data{
	color:#666;
	font-size:12px;
	line-height:21px;
	width:100%;
	margin:0 auto;
	text-align:left;
}
.datalist .data p{
	text-align:center;
}

</style>
'; ?>
